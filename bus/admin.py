from django.contrib import admin
from bus.models import User, Bus, Seat, Transaction

admin.site.register(Transaction)
admin.site.register(User)
admin.site.register(Bus)
admin.site.register(Seat)