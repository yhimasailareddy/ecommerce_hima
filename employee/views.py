from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .models import Employee,Employee_attendance

# Create your views here.
def Employee_list(request):
    employees = Employee.objects.all()
    return render(request, 'employee/emp.html', 
                  {'employees': employees})
#C for CREATE
def add_employee(request):
    if request.method == 'GET':
        return render(request, 'employee/add_employee.html')
    else:
        emp_name_ = request.POST['emp_name']
        emp_no_ = request.POST['emp_no']
        salary_ = request.POST['salary']

        s = Employee(
            emp_name = emp_name_,
            emp_no = emp_no_,
            salary = salary_,
        )
        s.save()
        return redirect('Employee_list')

#R for READ    
def employee_detail(request, id):
    s = Employee.objects.get(id=id)
    logged_in_username = request.session.get('username','')
    logged_in_userobject = User.objects.get(username=logged_in_username)
    if s.user != logged_in_userobject:
        return render(request,'employee/login.html',{'error': 'You are not allowed to view this page'})
    else:
        emp = Employee_attendance.objects.filter(employee = s)
        return render(request, 'employee/employee_details.html',{'employee': s,'emp':emp})

#D for DELETE
def delete_employee(request, id):
    s = Employee.objects.get(id=id)
    s.delete()
    return redirect('Employee_list')

#U for UPDATE
def update_employee(request, id):
    if request.method == 'GET':
        s = Employee.objects.get(id=id)
        editing = True
        context = {'employee': s, 'editing': editing}
        return render(request, 'employee/employee_details.html', context)

    if request.method == 'POST':
        s = Employee.objects.get(id=id)
        s.emp_name = request.POST['emp_name']
        s.salary = request.POST['salary']
        s.emp_no = request.POST['emp_no']
        s.save()
        return redirect('employee_detail', id)

    
def add_attendence(request,id):
    s = Employee.objects.get(id=id)
    new_date = request.POST['date']
    new_present = request.POST['present']
    s = Employee_attendance.objects.create(
        employee = s,
        date = new_date,
        present = new_present,
    )
    return redirect('employee_detail',id)
def edit_attendence(request,id,att_id):
    s = Employee_attendance.objects.get(id=att_id)
    if request.method == 'GET':
        return render(request, 'employee/attendence.html', {'att': s})
    elif request.method == 'POST':
        if request.POST.get('edit') == 'EDIT':
            s.date = request.POST['date']
            s.present = request.POST['present']
            s.save()
            return redirect('edit_attendence', id,att_id)
        else:
            s.delete()
            return redirect('employee_detail', id)
def login_view(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        u = authenticate(username=username, password=password)
        if u is not None:
            request.session['username'] = u.username
            corresponding_employee_object = Employee.objects.get(user=u)
            return redirect('employee_detail', corresponding_employee_object.id)
        else:
            del request.session['username']
            error = 'Wrong username or password'
            context['error'] = error
    return render(request, 'employee/login.html', context)
        
def logout_view(request):
    del request.session['username']
    return redirect('login')
