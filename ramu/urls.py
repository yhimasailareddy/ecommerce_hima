from django.urls import path
from ramu.views import chai_bandi_home, regular_tea, black_tea, add_stock, todays_earning

urlpatterns = [
    path('chai-bandi/', chai_bandi_home, name='chai_bandi_home'),
    path('regular-tea/', regular_tea, name='regular_tea'),
    path('black-tea/', black_tea, name='black_tea'),
    path('add-stock/', add_stock, name='add_stock'),
    path('todays-earning/', todays_earning, name='todays_earning'),
]
