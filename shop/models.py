from django.db import models

# Create your models here.
class Todo(models.Model):
    list_date = models.DateField()
    list_gorceries = [("low","low"), ("medium","medium"), ("high","high")]
    list_priority = models.CharField(max_length=10, choices=list_gorceries)

    def __str__(self):
        return f' {self.list_priority}-{self.list_date}'
    