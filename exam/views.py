from django.shortcuts import render
from exam.models import Item

# Create your views here.
def my_view(request):
    items = Item.objects.all()
    return render(request, 'exam/first.html',{'items': items})
