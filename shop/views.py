from django.shortcuts import render,redirect
from . models import Todo

# Create your views here.
def display(request):
    todo = Todo.objects.all()
    high_list = []
    medium_list = []
    low_list = []
    for item in todo:
        if item.list_priority == 'high':
            high_list.append(item)
        elif item.list_priority == 'medium':
            medium_list.append(item)
        else:
            low_list.append(item)
    new = high_list + medium_list + low_list
    return render(request, 'shop/show.html', {'todos': new})

def addto(request):
    if request.method == "GET":
          edit=True
          items=Todo.objects.all()
          return render(request,"shop/todo.html",{"items":items,"edit":edit})
    else:
        date=request.POST['data']
        k=request.POST['task']
        s = Todo(
            list_date=date,
            list_priority=k)
        s.save()
        return redirect('display')

    
