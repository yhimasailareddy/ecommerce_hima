from django.shortcuts import render,redirect
from .models import Employee,Employee_attendance
import datetime

# Create your views here.
def Employee_list(request):
    employees = Employee.objects.all()
    return render(request, 'office/employee_list.html', 
                  {'employees': employees})
#C for CREATE
def add_employee(request):
    if request.method == 'GET':
        return render(request, 'office/add_employee.html')
    else:
        emp_name_ = request.POST['emp_name']
        emp_no_ = request.POST['emp_no']
        salary_ = request.POST['salary']

        s = Employee(
            emp_name = emp_name_,
            emp_no = emp_no_,
            salary = salary_,
        )
        s.save()
        return redirect('Employee_list')

#R for READ    
def employee_detail(request, id):
    s = Employee.objects.get(id=id)
    emp = Employee_attendance.objects.filter(employee = s)
    return render(request, 'office/employee_details.html',
                  {'employee': s,'emp':emp})

#D for DELETE
def delete_employee(request, id):
    s = Employee.objects.get(id=id)
    s.delete()
    return redirect('Employee_list')

#U for UPDATE
def update_employee(request, id):
    if request.method == 'GET':
        s = Employee.objects.get(id=id)
        editing = True
        context = {'employee': s, 'editing': editing}
        return render(request, 'office/employee_details.html', context)

    if request.method == 'POST':
        s = Employee.objects.get(id=id)
        s.emp_name = request.POST['emp_name']
        s.salary = request.POST['salary']
        s.emp_no = request.POST['emp_no']
        s.save()
        return redirect('employee_detail', id)

    
def add_attendence(request,id):
    s = Employee.objects.get(id=id)
    new_date = request.POST['date']
    new_present = request.POST['present']
    s = Employee_attendance.objects.create(
        employee = s,
        date = new_date,
        present = new_present,
    )
    return redirect('employee_detail',id)
def edit_attendence(request,id,att_id):
    s = Employee_attendance.objects.get(id=att_id)
    if request.method == 'GET':
        return render(request, 'office/employee_attendence.html', {'att': s})
    elif request.method == 'POST':
        if request.POST.get('edit') == 'EDIT':
            s.date = request.POST['date']
            s.present = request.POST['present']
            s.save()
            return redirect('edit_attendence', id,att_id)
        else:
            s.delete()
            return redirect('employee_detail', id)