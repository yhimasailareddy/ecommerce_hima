from django.db import models

class Tea(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    stock = models.DecimalField(max_digits=6, decimal_places=2) 
    earnings = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return self.name

    def add_stock(self, quantity):
        self.stock += quantity
        self.save()

    def update_earnings(self, amount):
        self.earnings += amount
        self.save()
