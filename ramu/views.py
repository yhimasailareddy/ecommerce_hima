from django.shortcuts import render, redirect
from .models import Tea
from decimal import Decimal


def chai_bandi_home(request):
    teas = Tea.objects.all()
    return render(request, 'ramu/chai_bandi_home.html', {'teas': teas})


def regular_tea(request):
    milk = Tea.objects.get(name='Milk')
    tea_powder = Tea.objects.get(name='Tea Powder')
    milk.stock -= Decimal('0.15')  
    tea_powder.stock -= Decimal('0.01')  
    milk.save()
    tea_powder.save()
    today = date.today()
    earnings, created = Earning.objects.get_or_create(date=today)
    earnings.earnings += Decimal('15.00')
    earnings.save()
    teas = Tea.objects.all()  
    return render(request, 'ramu/chai_bandi_home.html', {'teas': teas})

def black_tea(request):
    tea_powder = Tea.objects.get(name='Tea Powder')
    tea_powder.stock -= Decimal('0.01')
    tea_powder.save()
    teas = Tea.objects.all()

    return render(request, 'ramu/chai_bandi_home.html', {'teas': teas})

def add_stock(request):
    if request.method == 'POST':
        tea_name = request.POST['tea']
        quantity = int(request.POST['quantity'])
        tea = Tea.objects.get(name=tea_name)
        tea.add_stock(quantity)
        return redirect('chai_bandi_home')
    
    teas = Tea.objects.all()
    return render(request, 'ramu/add_stock.html', {'teas': teas})


def todays_earning(request):
    teas = Tea.objects.all()
    total_earnings = sum(tea.earnings for tea in teas)
    return render(request, 'ramu/todays_earning.html', {'total_earnings': total_earnings})
