from django.urls import path,include
from bus import views

urlpatterns = [
    path('home/',views.home,name='home'),
    path('login/',views.login,name='login'),
    path('form-register/',views.registerForm,name='form-register'),
    path('register/', views.register,name='register'),
    path('form-bus/', views.busForm,name='form-bus'),
    path('form-add-bus/', views.addBusForm,name='form-add-bus'),
    path('add-bus/', views.addBus,name='add-bus'),
    path('delete-bus/<int:b_id>', views.deleteBus,name='delete-bus'),
    path('form-update-bus/<int:b_id>', views.formUpdateBus,name='form-update-bus'),
    path('update-bus/', views.updateBus,name='update-bus'),
    path('form-forgot-password/', views.formForgotPassword,name='form-forgot-password'),
    path('forgot-password/',views.forgotPassword,name='forgot-password'),

    path('form-search-bus/',views.formSearchBus,name='form-search-bus'),
    path('search-bus/',views.searchBus,name='search-bus'),

    path('seat/<int:b_id>', views.seat,name='seat'),
    path('transaction/<int:b_id>', views.transaction,name='transcation'),

    path('logout/',views.logout,name='logout'),
    path('form-change-password/',views.formChangePassword,name='form-change-password'),
    path('change-password/',views.changePassword,name='change-password')

]